<?php
ini_set('display_errors', 'Off');

header("Cache-Control: no-store, no-cache, must-revalidate"); // HTTP/1.1
header("Cache-Control: post-check=0, pre-check=0", false);
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
header("Pragma: no-cache"); // HTTP/1.0
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");


if($_POST['formSubmit'] == "Submit Form") {

//      echo("<p>dbInit</p>\n");
        $errorMessage = "";
        if(empty($_POST['RHULemail'])){
		$errorMessage .= "<li>Please enter your email address. Thank you.</li>";
        } else {

	$RHULemail    = $_POST['RHULemail'];
        

//      Init current db connection
        include("./db.php");

//      Check workerID is not in current database
        $o = "SELECT RHULemail FROM Sleep WHERE RHULemail = '$RHULemail'";
        $results = mysql_query($o);
        $numrows = mysql_numrows($results);
        for($j=0; $j<$numrows; $j++){
                $db_RHULemail = mysql_result($results,$j,"RHULemail");
        }
        if(!empty($db_RHULemail)){
                header("Location: idAlreadyRegistered.php?idnumber=".$idnumber);
                $errorMessage .= "<li>This email address has already been registered for this experiment. You may not take the experiment again. Thank you.</li>";
        } else {

	$condition1   = rand(1,3); //1 = control 2 and 3 = exp
	$condition2   = rand(1,2); //misinformation pre or post
	//($condition = 1) ? header("Location http://facebook.com") : header("Location: http://google.co.uk");
	// get time
	$dateTime1 	= date("Y-m-d H:i:s");
        $dateTime 	= strtotime("$dateTime1");

	$BEG_amTime	= "16:00:00"; //8am UK time experiment opens for AM control and wake experimental conditions
	$BEG_amTime	= strtotime("$BEG_amTime");
	$END_amTime	= "19:00:00"; //at 11am UK time experiment closes for AM control and wake experimental conditions
	$END_amTime     = strtotime("$END_amTime");

	$BEG_pmTime 	= "04:00:00"; //at  8pm  UK time experiment opens for PM control and sleep experimental conditions
	$BEG_pmTime     = strtotime("$BEG_pmTime");
	$END_pmTime     = "07:00:00"; //at 11pm UK time experiment closes for PM control and sleep experimental conditions
	$END_pmTime     = strtotime("$END_pmTime");

	if($dateTime > $BEG_amTime && $dateTime < $END_amTime){
		//AM CONDITION 

                        $q = "INSERT INTO `Sleep` (`RHULemail`,`dateTime`,`WScondition`,`Mcondition`) VALUES ('$RHULemail','$dateTime1','$condition1','$condition2')";
                        mysql_query($q,$s); //Insert email address into database
			
			
			if($condition1 == 1 && $condition2 == 1){ 
				header("Location: https://rhulpsychology.eu.qualtrics.com/jfe/form/SV_5A3hZpWWQj9aIBv");
			}else if($condition1 == 1 && $condition2 == 2){
				header("Location: https://rhulpsychology.eu.qualtrics.com/jfe/form/SV_7TFpzRaKlgeWxjT");
                        }else if($condition1 == 2 && $condition2 == 1){
				header("Location: https://rhulpsychology.eu.qualtrics.com/jfe/form/SV_5A3hZpWWQj9aIBv");
			}else if($condition1 == 3 && $condition2 == 1){
				header("Location: https://rhulpsychology.eu.qualtrics.com/jfe/form/SV_5A3hZpWWQj9aIBv");
			}else if($condition1 == 2 && $condition2 == 2){
				header("Location: https://rhulpsychology.eu.qualtrics.com/jfe/form/SV_7TFpzRaKlgeWxjT");
			}else if($condition1 == 3 && $condition2 == 2){
				header("Location: https://rhulpsychology.eu.qualtrics.com/jfe/form/SV_7TFpzRaKlgeWxjT");

}

	}else {
		 $errorMessage .= "<li>You have missed the opportunity to take part in this experiment at this time.</li>";
                 $errorMessage .= "<li>Please come back between 8 am - 11 am or 8pm - 11pm.</li>";
	}

	}

}

}


?>

<script type="text/javascript">
function onLoad() {
    document.getElementById('submits').addEventListener('keypress', function(event) {
        window.alert("Enter");
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

}

function openinput() {
    document.getElementById('submits').style.visibility = "visible";
}
</script>

<html>
<head>
<meta http-equiv="cache-control" content="no-cache"> <!-- tells browser not to cache -->
<meta http-equiv="Pragma" content="no-cache"> <!-- says not to use cached stuff -->
<meta http-equiv="Expires" content="0">
<link rel="stylesheet" type="text/css" href="./mlsite/ml.css">
</head>

<body onload="'#onLoad()'">
<form action="index1.php" method="post">
<table width="780" cellpadding="10" cellspacing="0" border="0" align="center">
<tr><td><h3>Human Memory Experiment</h3></td><td align="right"><img src="./logos/rhul.jpeg" width=150></tr>
</table>

<table width="780" cellpadding="10" cellspacing="0" border="0" align="center"><tr><td>
<?php
        if(!empty($errorMessage)) {
                echo("<p>There was an error with your form:</p>\n");
                echo("<ul>" . $errorMessage . "</ul>\n");
        } 

?>
<p>Please note: 
<ul><li>This experiment may be split into two parts. You may be asked to come back at a later time. We will tell you at the beginning of the experiment.</li>
<!-- <li>There will also be several eligibity questions before you begin the experiment.</li> -->
<li>Please fill out the form below.</li>
<!-- <//</ul>

</p> -->


<p>Please enter your email address: &nbsp &nbsp<br>
<input size=30 type="text" name="RHULemail" id="RHULemail" maxlength="100"/></p>

<p>Click here if you are ready to submit the form: <input type="checkbox" name="agree" onClick="openinput()" value="agree" /> </p>
<p>
<input type="submit" name="formSubmit" id="submits" value="Submit Form" style="visibility: hidden">
</td>
</tr>
</table>
</body>
</html>
